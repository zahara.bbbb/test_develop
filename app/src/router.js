import Router from 'koa-router';
import { wrapper } from 'koa-swagger-decorator';

const router = new Router();
//if (process.env.NODE_ENV === 'development') {
wrapper(router);
router.swagger({
  title: 'SERVER',
  description: 'API DOC',
  version: '1.0.0',
});
//}

export default router;
