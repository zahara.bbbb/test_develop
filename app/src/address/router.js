import Router from 'koa-router';

import AddressContreoller from './address';
import {Auth, } from '../../middlewares';

const router = new Router({ prefix: '/address' });

router
  .post('/checkAddress', Auth,  AddressContreoller.checkAddress);

export default router.routes();
