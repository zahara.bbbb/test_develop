export CheckFilter from './check.filter.json';

export const swaggerCheckAddress = {
  country: { type: 'string', required: false },
  city: { type: 'string', required: false },
  zip: { type: 'string', required: false },
  street: { type: 'string', required: false },
  state: { type: 'string', required: false },
};
