import { body, request, summary, tags } from 'koa-swagger-decorator';
import { PasswordUtil, validator } from '../../utils';
import Boom from 'boom';
import { AttomDataService, SearchrequestService } from '../../services';
import { CheckFilter, swaggerCheckAddress } from './schemas';
const tag = tags(['address']);

export default class AddressContreoller {
  @request('post', '/address/checkAddress')
  @summary('Check if address')
  @body(swaggerCheckAddress)
  @validator(CheckFilter)
  @tag
  static async checkAddress(ctx, next) {
    const { request: { body: { country, city, zip, street, state } } } = ctx;
    let attomResponse;
    try {
      attomResponse = await AttomDataService.SalesComparables(country, city, zip, street, state);
    } catch (e) {
      throw Boom.badRequest('Something goes wrong.');
    }

    if (attomResponse.status !== 200 || !attomResponse.data) { throw Boom.badRequest('Something goes wrong.'); }

    const user = ctx.state.user;

    const infoexit = {
      userid: user.id,
      status: attomResponse.status,
      filterid: 0,
      responsejson: JSON.stringify(attomResponse.data),
    };

    const saveResult = await SearchrequestService.createDBResponseJson(infoexit);

    ctx.status = 200;
    ctx.body = { saveResult };
    await next();
  }
}
