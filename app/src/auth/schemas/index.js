export loginSchema from './login.schema.json';
export registerSchema from './register.schema.json';
export checkSchema from './check.schema.json';

export const swaggerLoginSchema = {
  username: { type: 'string', required: true },
  password: { type: 'string', required: true },
  code: { type: 'string' },
};

export const swaggerRegisterSchema = {
  username: { type: 'string', required: true },
  password: { type: 'string', required: true },
};

export const swaggerCheckSchema = {
  username: { type: 'string', required: true },
};
