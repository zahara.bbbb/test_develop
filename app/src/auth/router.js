import Router from 'koa-router';

import AuthController from './auth.controller';
import {Auth, } from '../../middlewares';

const router = new Router({ prefix: '/auth' });

router
  .post('/login',  AuthController.login)
  .post('/register', AuthController.register)
  .post('/check_username', AuthController.checkUsername)
  .get('/me', Auth, AuthController.me);

export default router.routes();
