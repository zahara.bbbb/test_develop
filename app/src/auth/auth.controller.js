import Boom from 'boom';
import { request, body, tags, summary, security } from 'koa-swagger-decorator';
import {
  loginSchema,
  registerSchema,
  checkSchema,
  swaggerCheckSchema,
  swaggerLoginSchema,
  swaggerRegisterSchema
} from './schemas';
import {
  validator,
  PasswordUtil,
  TokenUtil,
  Errors
} from './../../utils';
import {  UserService } from '../../services';

const tag = tags(['auth']);
const NOT_FOUND = Errors.MODEL_NOT_FOUND.message('User');

export default class AuthContreoller {
  @request('post', '/auth/login')
  @summary('Get auth token')
  @body(swaggerLoginSchema)
  @validator(loginSchema)
  @tag
  static async login(ctx, next) {
    const { request: { body: { password, username } } } = ctx;

    const user = await UserService.getUserByUsername(username, []);
    if (!user) throw Boom.notFound(NOT_FOUND);

    if (!PasswordUtil.comparePassword(user.password, password)) {
      throw Boom.notFound(NOT_FOUND);
    }

    const token = TokenUtil.generate({ userId: user.id });
    ctx.body = { token, user };

    await next();
  }

  @request('post', '/auth/register')
  @summary('Create new user and send activation code to username')
  @body(swaggerRegisterSchema)
  @validator(registerSchema)
  @tag
  static async register(ctx, next) {
    const { request: { body: { username, password } } } = ctx;
    if (!username.match('^[a-zA-Z0-9_.-]{4,}$'))
      throw Boom.methodNotAllowed('Validation error. Check Username.');
    if (!password.match('^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#\\$%\\^\\&*\\)\\(+=._-]{8,}$'))
      throw Boom.methodNotAllowed('Validation error. Check Password.');
    const usernameExists = await UserService.getUserByUsername(username);
    if (usernameExists) throw Boom.badRequest('This username already exists.');

    const userData = {
      username,
      password: PasswordUtil.saltHashPassword(password)
    };
    const user = await UserService.createUser(userData);
    const token = TokenUtil.generate({ userId: user.id });

    ctx.status = 201;
    ctx.body = { token, user };
    await next();
  }

  @request('post', '/auth/check_username')
  @summary('Check if username is stored')
  @body(swaggerCheckSchema)
  @validator(checkSchema)
  @tag
  static async checkUsername(ctx, next) {
    const { request: { body: { username } } } = ctx;
    const usernameExists = await UserService.getUserByUsername(username);
    if (usernameExists) throw Boom.badRequest('This username already taken.');
    ctx.status = 201;
    ctx.body = { result: 'This username is free', username: username};
    await next();
  }

  @security([{ ApiKeyAuth: [] }])
  @request('get', '/auth/me')
  @summary('Get user data by token')
  @tag
  static async me(ctx, next) {
    ctx.status = 200;
    const user = ctx.state.user;
    const token = await TokenUtil.generate({ userId: user.id });
    ctx.body = { token, user };
    await next();
  }
}
