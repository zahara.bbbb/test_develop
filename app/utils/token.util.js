import jwt from 'jsonwebtoken';
import { USER_TOKEN_CONFIG, ACCESS_TOKEN_CONFIG } from '../config/token.config';

export default class TokenUtil {
  /**
   * Generate token from data
   * @param data - data
   * @param access {boolean}
   * @returns {string} - encoded value, token
   */
  static generate(data, access = false) {
    const TOKEN_CONFIG = this.getConfig(access);
    return jwt.sign(data, TOKEN_CONFIG.KEY, {
      expiresIn: TOKEN_CONFIG.expires,
      algorithm: TOKEN_CONFIG.alg,
    });
  }

  /**
   * Decode given payload (usually, authorization field from request header)
   * @param payload - string with token to decode
   * @param access {boolean}
   * @returns {object|null} - object with users data
   */
  static decode(payload, access = false) {
    const TOKEN_CONFIG = this.getConfig(access);
    try {
      return jwt.verify(payload, TOKEN_CONFIG.KEY);
    } catch (e) {
      return null;
    }
  }

  static getConfig(access = false) {
    if (access) return ACCESS_TOKEN_CONFIG;
    return USER_TOKEN_CONFIG;
  }
}
