
const TOKEN_NOT_VALID = {
  success: false,
  message: 'Token is not valid',
};
const TOKEN_NOT_PROVIDED = {
  success: false,
  message: 'Token is not provided',
};
const PERMISSION_DENIED = {
  success: false,
  message: 'Permission denied',
};
const ACCOUNT_NOT_ACTIVATE = {
  success: false,
  message: 'Account is not activate',
};
const APP_NOT_ACTIVATE = {
  success: false,
  message: 'App is not activate',
};
const MOONPAY_SIGNATURE = {
  success: false,
  message: 'Signatures aren\'t match',
};

const MODEL_NOT_FOUND = {
  success: false,
  message: model => `${model} not found`,
};

const BAD_REQUEST = {
  success: false,
  message: 'Bad Request',
};

export default {
  TOKEN_NOT_VALID,
  TOKEN_NOT_PROVIDED,
  PERMISSION_DENIED,
  ACCOUNT_NOT_ACTIVATE,
  APP_NOT_ACTIVATE,
  MOONPAY_SIGNATURE,
  MODEL_NOT_FOUND,
  BAD_REQUEST
};

