const axios = require('axios');
import {ATTOM_DATA_API_TOKEN} from '../config/token.config';

export default class AttomDataService {
  static async SalesComparables (country, city, zip, street, state) {
    const countryEncoded = encodeURIComponent(country);
    const streetEncoded = encodeURIComponent(street);
    const cityEncoded = encodeURIComponent(city);
    const stateEncoded = encodeURIComponent(state);
    const zipEncoded = encodeURIComponent(zip);

    const params = {
      searchType: 'Radius',
      minComps: 1,
      maxComps: 6,
      miles: 5,
      bedroomsRange: 2,
      bathroomRange: 2,
      sqFeetRange: 100,
      lotSizeRange: 15000,
      saleDateRange: 32,
      yearBuiltRange: 40,
      ownerOccupied: 'Both',
      distressed: 'IncludeDistressed',
      saleAmountRangeFrom: 50000,
    };

    let url = 'https://api.attomdata.com/property/v2/salescomparables/address/';
    url += `${streetEncoded}/${cityEncoded}/${countryEncoded}/${stateEncoded}/${zipEncoded}`;
    url += `?${this.serialize(params)}`;

    const options = {
      method: 'get',
      headers: {
        Accept: 'application/json',
        APIKey: ATTOM_DATA_API_TOKEN,
      },
    };

    try {
      return await axios.get(url, options);
    } catch (e) {
      console.log('Attomdata service', e);
      throw e;
      // TODO: error handlers
    }
  }
  static serialize (obj) {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(`${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`);
      }
    }
    return str.join('&');
  }
}
