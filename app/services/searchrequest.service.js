import db from '../models';

export default class SearchrequestService {
  static async createDBResponseJson(searchData) {
    return db.searchrequest.create(searchData);
  }
}
