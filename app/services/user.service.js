import db from '../models';
const Sequelize = require('sequelize');

export default class UserService {
  static async getUser(id, exclude = ['password', 'words']) {
    return db.user.findOne({ where: { id }, attributes: { exclude }});
  }

  static async getAllUsers(exclude = ['password', 'words']) {
    return db.user.findAll({ attributes: { exclude } });
  }

  static async createUser(userData, exclude = ['password', 'words']) {
    return db.user.create(userData, { attributes: { exclude }});
  }

  static async getUserByUsername(username, exclude = ['password', 'words']) {
    console.log(username);
    return db.user.findOne({ where: { username: {[Sequelize.Op.iLike]: username} }, exclude});
  }

  static async getUserByMoonId(moonId, exclude = ['password', 'words']) {
    return db.user.findOne({ where: { moonId: moonId }, attributes: { exclude } });

  }
}
