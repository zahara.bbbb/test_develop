'use strict';
module.exports= (sequelize, DataTypes) => {
  const transaction = sequelize.define('transaction', {
    requesid: { type: DataTypes.INTEGER, allowNull: false},
    status: { type: DataTypes.STRING(255), allowNull: false},
    provider: {type: DataTypes.STRING(255), allowNull: false},
    foreingid: {type: DataTypes.STRING(255), allowNull: false}
  }, {});
  return transaction;
};
