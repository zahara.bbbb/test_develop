'use strict';
module.exports = (sequelize, DataTypes) => {
  const address = sequelize.define('address', {
    requestid: { type: DataTypes.INTEGER, allowNull: false, unique: true},
    role: { type: DataTypes.STRING(255), allowNull: false },
    street: { type: DataTypes.STRING(255),allowNull: false },
    street2: { type: DataTypes.STRING(255),allowNull: true },
    city: { type: DataTypes.STRING(255),allowNull: false },
    state: { type: DataTypes.STRING(255),allowNull: true },
    country: { type: DataTypes.STRING(255),allowNull: false },
    zip: { type: DataTypes.STRING(255),allowNull: false },
    parcelid: {type: DataTypes.STRING(255),allowNull: true },
    miles: {type: DataTypes.STRING(255), allowNull: true },
    sqsize: {type: DataTypes.INTEGER, allowNull: true },
    sqlot: {type: DataTypes.INTEGER, allowNull: true },
    geoLatitude: {type: DataTypes.STRING(255), allowNull: true },
    geoLongitude: {type: DataTypes.STRING(255),allowNull: true },
    saledate: {type: DataTypes.DATE,allowNull: true },
    saleprice: {type: DataTypes.STRING(255),allowNull: true },
    yearbuilt: {type: DataTypes.INTEGER, allowNull: true },
    bathscount: {type: DataTypes.INTEGER, allowNull: true },
    beadscount:{type: DataTypes.INTEGER, allowNull: true },
    mortgageholder: {type: DataTypes.STRING(255),allowNull: true }
  }, {});
  return address;
};
