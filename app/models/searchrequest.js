'use strict';
module.exports = (sequelize, DataTypes) => {
  const searchrequest = sequelize.define('searchrequest', {
    userid: { type: DataTypes.INTEGER, allowNull: false},
    status: { type: DataTypes.STRING(255), allowNull: false },
    filterid: {type: DataTypes.INTEGER,allowNull: false},
    responsejson: {type: DataTypes.STRING(255),allowNull: false}
  }, {});
  return searchrequest;
};
