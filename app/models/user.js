'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    username: { type: DataTypes.STRING(255), allowNull: false, unique: true},
    password: { type: DataTypes.STRING(255), allowNull: false }
  }, {});
  return user;
};
