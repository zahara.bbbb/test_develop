'use strict';
module.exports = (sequelize, DataTypes) => {
  const filter = sequelize.define('filter', {
    username: { type: DataTypes.STRING(255), allowNull: false },
    value: { type: DataTypes.STRING(255), allowNull: false }
  }, {});
  return filter;
};
