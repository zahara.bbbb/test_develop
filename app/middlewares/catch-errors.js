import _debugger from 'debug';

const error = _debugger('bitholder-api:error');

export default async (ctx, next) => {
  try {
    await next();
    console.log("IP", ctx.request.ip);
  } catch (e) {
    error('Catched error: ', e);
    let payload = e;
    if (e.isBoom) {
      payload = e.output.payload;
      payload.data = e.data;
    }
    ctx.status = payload.statusCode || payload.status || 500;
    console.log("IP", ctx.request.ip);
    console.log(e);
    ctx.body = payload;
  }
};
