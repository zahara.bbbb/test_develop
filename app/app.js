import fs from 'fs';
import _debugger from 'debug';

import Koa from 'koa';
import cors from 'koa-cors';
import logger from 'koa-logger';
import koaBody from 'koa-body';
import convert from 'koa-convert';
import { CatchErrors } from './middlewares';
import router from './src/router';
const KoaStatic = require('koa-static');
const error = _debugger('koa2-starter:error');
const debug = _debugger('koa2-starter:debug');

const app = new Koa();

app
  .use(CatchErrors)
  .use(router.routes())
  .use(KoaStatic(`${__dirname}/public`))
  .use(convert(cors({ origin: true })))
  .use(logger())
  .use(convert(koaBody({ jsonLimit: '30mb', multipart: true })))
;

/**
 * Init all routes.
 */
fs.readdirSync(`${__dirname}/src`)
  .forEach((mod) => {
    try {
      if (mod === 'router.js') return;
      app.use(require(`${__dirname}/src/${mod}/router.js`).default) // eslint-disable-line
      debug(`loaded: '${mod}' module.`);
      console.log(`loaded: '${mod}' module.`);
    } catch (e) {
      error(`Error, while loading ${mod}`, e);
      console.error(`Error, while loading ${mod}`, e);
    }
  });
module.exports = app;
